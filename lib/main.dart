import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:test_majoo/static/app_information.dart';
import 'package:test_majoo/static/app_theme.dart';

import 'core/views/splashscreen/splashscreen_view.dart';

class MyHttpoverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

main() async {
  HttpOverrides.global = new MyHttpoverrides();
  WidgetsFlutterBinding.ensureInitialized();
  setupRunApp();
}

setupRunApp() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(
      GetMaterialApp(
        defaultTransition: Transition.fadeIn,
        builder: (BuildContext context, Widget? child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: child!,
          );
        },
        title: AppInformation.appName,
        darkTheme: AppThemes().darkTheme,
        theme: AppThemes().lightTheme,
        themeMode: ThemeMode.light,
        debugShowCheckedModeBanner: false,
        home: SplashscreenView(),
      ),
    );
  });
}
