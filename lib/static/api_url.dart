class ApiUrl {
  static String baseUrl = "https://api.pexels.com/v1";

  static String curated = baseUrl + "/curated";
}
