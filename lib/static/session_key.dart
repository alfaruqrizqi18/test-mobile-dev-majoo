class SessionKey {
  static String isLoggedIn = "is_logged_in";
  static String userId = "user_id";
  static String username = "username";
  static String name = "name";
}
