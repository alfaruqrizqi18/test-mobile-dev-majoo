import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'color_app.dart';

class AppThemes {
  static String _fontFamily = "Poppins";
  final darkTheme = ThemeData.dark().copyWith(
    primaryColor: Colors.black,
    brightness: Brightness.dark,
    backgroundColor: const Color(0xFF212121),
    accentIconTheme: IconThemeData(color: Colors.black),
    appBarTheme: AppBarTheme(brightness: Brightness.dark),
    dividerColor: Colors.white30,
    textTheme: ThemeData.dark().textTheme.apply(fontFamily: _fontFamily),
    primaryTextTheme: ThemeData.dark().textTheme.apply(fontFamily: _fontFamily),
    accentTextTheme: ThemeData.dark().textTheme.apply(fontFamily: _fontFamily),
    primaryIconTheme: IconThemeData(color: Colors.white),
    accentColor: ColorApp.mainColorApp,
    hintColor: ColorApp.mainColorApp,
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );

  final lightTheme = ThemeData.light().copyWith(
    primaryColor: Colors.white,
    backgroundColor: const Color(0xFFE5E5E5),
    accentIconTheme: IconThemeData(color: Colors.black),
    appBarTheme: AppBarTheme(brightness: Brightness.light),
    dividerColor: Colors.grey[300],
    textTheme: ThemeData.light().textTheme.apply(fontFamily: _fontFamily),
    primaryTextTheme: ThemeData.light().textTheme.apply(fontFamily: _fontFamily),
    accentTextTheme: ThemeData.light().textTheme.apply(fontFamily: _fontFamily),
    primaryIconTheme: IconThemeData(color: Colors.black),
    brightness: Brightness.light,
    accentColor: ColorApp.mainColorApp,
    hintColor: ColorApp.mainColorApp,
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );

  final systemUIOverlayLight = SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
    systemNavigationBarColor: Colors.black,
    systemNavigationBarIconBrightness: Brightness.light,
    systemNavigationBarDividerColor: Colors.white,
  );

  final systemUIOverlayDark = SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarColor: Colors.white,
    systemNavigationBarIconBrightness: Brightness.dark,
    systemNavigationBarDividerColor: Colors.black,
  );
}
