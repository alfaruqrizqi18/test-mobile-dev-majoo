import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_majoo/core/controllers/main_pages/profile/profile_controller.dart';
import 'package:test_majoo/static/color_app.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ProfileController>();
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0.8,
            centerTitle: true,
            title: Text(
              "Profile",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            actions: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      right: 15,
                    ),
                    child: GestureDetector(
                      onTap: () {
                        controller.logout();
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.red[100],
                        child: Icon(
                          Icons.logout,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          body: ListView(
            padding: EdgeInsets.only(
              top: 15,
              right: 25,
              left: 25,
            ),
            children: [
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.person,
                    color: ColorApp.mainColorApp,
                  ),
                  backgroundColor: ColorApp.mainColorApp.withOpacity(0.2),
                ),
                title: Text(controller.userDetail['name']),
                subtitle: Text(controller.userDetail['username']),
              )
            ],
          ),
        );
      },
    );
  }
}
