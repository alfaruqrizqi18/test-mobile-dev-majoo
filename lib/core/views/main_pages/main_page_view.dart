import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_majoo/core/controllers/main_pages/main_page_controller.dart';
import 'package:test_majoo/static/color_app.dart';

class MainPageView extends StatefulWidget {
  const MainPageView({Key? key}) : super(key: key);

  @override
  _MainPageViewState createState() => _MainPageViewState();
}

class _MainPageViewState extends State<MainPageView>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(MainPageController());
    super.build(context);
    return GetBuilder(
        init: controller,
        builder: (_) {
          return Scaffold(
            bottomNavigationBar: BottomNavigationBar(
              showUnselectedLabels: true,
              currentIndex: controller.selectedIndexPage,
              selectedItemColor: ColorApp.mainColorApp,
              unselectedItemColor: Colors.grey[400],
              type: BottomNavigationBarType.fixed,
              selectedLabelStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 11,
              ),
              unselectedLabelStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 11,
              ),
              onTap: (index) {
                controller.changePage(index);
              },
              items: [
                BottomNavigationBarItem(
                  icon: new Icon(Icons.home),
                  label: "Beranda",
                ),
                BottomNavigationBarItem(
                  icon: new Icon(Icons.favorite_border_outlined),
                  label: "Favorit",
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  label: "Profil",
                )
              ],
            ),
            body: controller.pages[controller.selectedIndexPage],
          );
        });
  }

  @override
  bool get wantKeepAlive => true;
}
