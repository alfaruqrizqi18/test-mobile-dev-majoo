import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_majoo/core/controllers/main_pages/favorites/favorites_controller.dart';
import 'package:test_majoo/static/color_app.dart';

class FavoritesView extends StatelessWidget {
  const FavoritesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<FavoritesController>();
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Scaffold(
          floatingActionButton: Container(
            margin: EdgeInsets.only(
              bottom: 15,
              right: 15,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  child: FloatingActionButton(
                    heroTag: "change_view",
                    backgroundColor: ColorApp.mainColorApp,
                    onPressed: () {
                      controller.changeView();
                    },
                    child: controller.isListView
                        ? Icon(
                            Icons.grid_view_rounded,
                          )
                        : Icon(
                            Icons.list,
                          ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child: FloatingActionButton(
                    heroTag: "change_sort",
                    backgroundColor: ColorApp.mainColorApp,
                    onPressed: () {
                      controller.changeSort();
                    },
                    child: controller.isDesc
                        ? Icon(
                            Icons.arrow_upward_rounded,
                          )
                        : Icon(
                            Icons.arrow_downward_rounded,
                          ),
                  ),
                ),
              ],
            ),
          ),
          appBar: AppBar(
            elevation: 0.8,
            centerTitle: true,
            title: Text(
              "Favorites",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            actions: [],
          ),
          body: RefreshIndicator(
            onRefresh: () async {
              controller.getFavoriteImages();
            },
            child: ListView(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.1,
              ),
              physics: BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              children: [
                ListTile(
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 25,
                  ),
                  leading: CircleAvatar(
                    backgroundColor: ColorApp.mainColorApp.withOpacity(0.2),
                    child: Icon(
                      Icons.info_outline_rounded,
                      color: ColorApp.mainColorApp,
                    ),
                  ),
                  title: Text(
                    "Sekilas Info",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  subtitle: Text(
                    "Double tap untuk menghapus foto favorite",
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey,
                    ),
                  ),
                ),
                controller.isListView
                    ? ListView.builder(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        padding: EdgeInsets.only(
                          top: 15,
                        ),
                        itemCount: controller.images.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onDoubleTap: () {
                              controller.removeFromFavorite(
                                imageId: controller.images[index]['id'],
                              );
                            },
                            child: ListTile(
                              contentPadding: EdgeInsets.only(
                                left: 25,
                                right: 25,
                              ),
                              title: Text(
                                controller.images[index]['alt'].toString(),
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              trailing: Column(
                                children: [
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.1,
                                    height:
                                        MediaQuery.of(context).size.width * 0.1,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                          controller.images[index]['small']
                                              .toString(),
                                        ),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      )
                    : GridView.builder(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        padding: EdgeInsets.only(),
                        itemCount: controller.images.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          childAspectRatio: 1,
                        ),
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onDoubleTap: () {
                              controller.removeFromFavorite(
                                imageId: controller.images[index]['id'],
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(
                                    controller.images[index]['medium']
                                        .toString(),
                                  ),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
              ],
            ),
          ),
        );
      },
    );
  }
}
