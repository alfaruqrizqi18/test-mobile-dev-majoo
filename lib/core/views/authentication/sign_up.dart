import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_majoo/core/controllers/authentication/sign_up_controller.dart';
import 'package:test_majoo/static/app_information.dart';
import 'package:test_majoo/static/app_theme.dart';
import 'package:test_majoo/static/color_app.dart';
import 'package:test_majoo/widgets/button.dart';

class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(SignUpController());
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Scaffold(
          bottomNavigationBar: Container(
            alignment: Alignment.center,
            height: 50,
            child: RichText(
              text: TextSpan(children: <WidgetSpan>[
                WidgetSpan(
                  child: Text(
                    "${AppInformation.appName}",
                    style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                    ),
                  ),
                ),
              ]),
            ),
          ),
          appBar: AppBar(
            backgroundColor: AppThemes().lightTheme.scaffoldBackgroundColor,
            elevation: 0,
          ),
          body: SafeArea(
            child: ListView(
              padding: EdgeInsets.only(
                right: 20,
                left: 20,
                top: 30,
              ),
              physics: BouncingScrollPhysics(),
              children: [
                Container(
                  margin: EdgeInsets.only(right: 20.0, left: 20.0, bottom: 40),
                  child: ListTile(
                    contentPadding: EdgeInsets.zero,
                    dense: true,
                    title: Text(
                      'Registrasi,',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 26,
                      ),
                    ),
                    subtitle: RichText(
                      text: TextSpan(children: <WidgetSpan>[
                        WidgetSpan(
                          child: Text(
                            "Rizqi Alfaruq ",
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                            ),
                          ),
                        ),
                        WidgetSpan(
                          child: Text(
                            "${AppInformation.appName}",
                            style: TextStyle(
                              color: ColorApp.mainColorApp,
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                            ),
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
                Container(
                  margin:
                      const EdgeInsets.only(top: 0.0, right: 20.0, left: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Nama",
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w600,
                            fontSize: 14.0),
                      ),
                      Container(
                          transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                          child: Theme(
                              child: TextFormField(
                                controller: controller.nameController,
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.left,
                                maxLines: 1,
                                cursorColor: ColorApp.mainColorApp,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),
                                obscureText: false,
                                decoration: InputDecoration(
                                    disabledBorder: UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    enabledBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    focusedBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    hintText: "Username disini...",
                                    hintStyle: TextStyle(color: Colors.grey)),
                              ),
                              data: Theme.of(context).copyWith(
                                primaryColor: ColorApp.mainColorApp,
                              ))),
                    ],
                  ),
                ),
                Container(
                  margin:
                      const EdgeInsets.only(top: 0.0, right: 20.0, left: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Username",
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w600,
                            fontSize: 14.0),
                      ),
                      Container(
                          transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                          child: Theme(
                              child: TextFormField(
                                controller: controller.usernameController,
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.left,
                                maxLines: 1,
                                cursorColor: ColorApp.mainColorApp,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),
                                obscureText: false,
                                decoration: InputDecoration(
                                    disabledBorder: UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    enabledBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    focusedBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    hintText: "Username disini...",
                                    hintStyle: TextStyle(color: Colors.grey)),
                              ),
                              data: Theme.of(context).copyWith(
                                primaryColor: ColorApp.mainColorApp,
                              ))),
                    ],
                  ),
                ),
                Container(
                  margin:
                      const EdgeInsets.only(top: 0.0, right: 20.0, left: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Katasandi",
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w600,
                            fontSize: 14.0),
                      ),
                      Container(
                          transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                          child: Theme(
                              child: TextFormField(
                                controller: controller.passwordController,
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.left,
                                maxLines: 1,
                                cursorColor: ColorApp.mainColorApp,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),
                                obscureText:
                                    controller.isPasswordVisible ? false : true,
                                decoration: InputDecoration(
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        controller.changeVisibilityPassword();
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 10),
                                        child: Icon(
                                          Icons.remove_red_eye,
                                          color: controller.isPasswordVisible
                                              ? ColorApp.mainColorApp
                                              : Colors.grey,
                                        ),
                                      ),
                                    ),
                                    disabledBorder: UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    enabledBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    focusedBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.transparent)),
                                    hintText: "Katasandi disini...",
                                    hintStyle: TextStyle(color: Colors.grey)),
                              ),
                              data: Theme.of(context).copyWith(
                                primaryColor: ColorApp.mainColorApp,
                              ))),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                  alignment: Alignment.center,
                  child: AppButton().mainButton(
                    textButton: "Sign up",
                    function: () {
                      controller.validation();
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
