import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_majoo/core/controllers/splashscreen/splashscreen_controller.dart';
import 'package:test_majoo/static/app_information.dart';

class SplashscreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(SplashscreenController());
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Scaffold(
          body: Center(
            child: Container(
              margin: EdgeInsets.only(
                top: 30,
              ),
              child: Text(
                AppInformation.appName.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
