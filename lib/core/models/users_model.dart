class UsersModel {
  late int id;
  late String name;
  late String username;
  late String password;

  UsersModel({
    required this.id,
    required this.name,
    required this.username,
    required this.password,
  });

  UsersModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'].toString();
    username = json['username'].toString();
    password = json['password'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}
