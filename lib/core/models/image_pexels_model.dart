class ImagePexelsModel {
  late final int id;
  late final int width;
  late final int height;
  late final String url;
  late final String photographer;
  late final String photographerUrl;
  late final int photographerId;
  late final String avgColor;
  late final String original;
  late final String large2x;
  late final String large;
  late final String medium;
  late final String small;
  late final String portrait;
  late final String landscape;
  late final String tiny;
  late final String liked;
  late final String alt;

  ImagePexelsModel({
    required this.id,
    required this.width,
    required this.height,
    required this.url,
    required this.photographer,
    required this.photographerUrl,
    required this.photographerId,
    required this.avgColor,
    required this.original,
    required this.large2x,
    required this.large,
    required this.medium,
    required this.small,
    required this.portrait,
    required this.landscape,
    required this.tiny,
    required this.liked,
    required this.alt,
  });

  ImagePexelsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    width = json['width'];
    height = json['height'];
    url = json['url'];
    photographer = json['photographer'];
    photographerUrl = json['photographer_url'];
    photographerId = json['photographer_id'];
    avgColor = json['avg_color'];
    original = json['original'].toString();
    large2x = json['large2x'].toString();
    large = json['large'].toString();
    medium = json['medium'].toString();
    small = json['small'].toString();
    portrait = json['portrait'].toString();
    landscape = json['landscape'].toString();
    tiny = json['tiny'].toString();
    liked = json['liked'].toString();
    alt = json['alt'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['width'] = width;
    _data['height'] = height;
    _data['url'] = url;
    _data['photographer'] = photographer;
    _data['photographer_url'] = photographerUrl;
    _data['photographer_id'] = photographerId;
    _data['avg_color'] = avgColor;
    _data['original'] = original;
    _data['large2x'] = large2x;
    _data['large'] = large;
    _data['medium'] = medium;
    _data['small'] = small;
    _data['portrait'] = portrait;
    _data['landscape'] = landscape;
    _data['tiny'] = tiny;
    _data['liked'] = liked;
    _data['alt'] = alt;
    return _data;
  }
}
