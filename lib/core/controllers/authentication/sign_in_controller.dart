import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/core/models/users_model.dart';
import 'package:test_majoo/core/views/authentication/sign_in.dart';
import 'package:test_majoo/core/views/main_pages/main_page_view.dart';
import 'package:test_majoo/services/db_services.dart';
import 'package:test_majoo/static/session_key.dart';

class SignInController extends GetxController {
  //controller username
  TextEditingController usernameController = TextEditingController();

  //controller password
  TextEditingController passwordController = TextEditingController();

  //boolean status apakah password bisa dilihat
  bool isPasswordVisible = false;

  @override
  void onInit() {
    super.onInit();
  }

  changeVisibilityPassword() {
    isPasswordVisible = !isPasswordVisible;
    //update adalah method bawaan dari Framework GetX untuk mengupdate state
    update();
  }

  validation() async {
    if (usernameController.text.isEmpty || passwordController.text.isEmpty) {
      return Get.snackbar(
        "Oops!",
        "Mohon lengkapi username ataupun katasandi",
        backgroundColor: Colors.red[400],
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING,
        icon: Icon(
          Icons.warning,
          color: Colors.white,
        ),
        isDismissible: true,
        margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
      );
    } else {
      signIn();
    }
  }

  signIn() async {
    Database db = await DBServices.instance.database;
    var res = await db.rawQuery(
      '''
      select * from ${DBServices.userTable} 
      where username = '${usernameController.text.toString()}'
      and password = '${passwordController.text.toString()}'
      ''',
    );
    if (res.isNotEmpty) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      UsersModel usersModel = UsersModel.fromJson(res.toList()[0]);
      prefs.setBool(SessionKey.isLoggedIn, true);
      prefs.setInt(SessionKey.userId, usersModel.id);
      prefs.setString(SessionKey.name, usersModel.name.toString());
      prefs.setString(SessionKey.username, usersModel.username.toString());
      Get.offAll(() => MainPageView());
    } else {
      Get.snackbar(
        "Oops!",
        "Username atau katasandi mungkin salah",
        backgroundColor: Colors.red,
        colorText: Colors.white,
        duration: Duration(seconds: 3),
      );
    }
  }

  removeSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    Get.offAll(() => SignIn());
  }
}
