import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/core/models/users_model.dart';
import 'package:test_majoo/services/db_services.dart';

class SignUpController extends GetxController {
  final nameController = TextEditingController();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  //boolean status apakah password bisa dilihat
  bool isPasswordVisible = false;

  validation() {
    if (nameController.text.isEmpty ||
        usernameController.text.isEmpty ||
        passwordController.text.isEmpty) {
      Get.snackbar(
        "Oops!",
        "Form tidak boleh kosong",
        backgroundColor: Colors.red,
        colorText: Colors.white,
        duration: Duration(seconds: 3),
      );
    } else {
      // add();
      checkIsUserExist();
    }
  }

  add() async {
    await DBServices.instance.insert(
      tableName: DBServices.userTable,
      models: UsersModel(
        id: Random().nextInt(999),
        name: nameController.text.toString(),
        username: usernameController.text.toString(),
        password: passwordController.text.toString(),
      ).toJson(),
    );
    nameController.clear();
    usernameController.clear();
    passwordController.clear();
    Get.snackbar(
      "Yeay!",
      "Berhasil registrasi user baru",
      backgroundColor: Colors.green[300],
      colorText: Colors.white,
      duration: Duration(seconds: 3),
    );
  }

  checkIsUserExist() async {
    Database db = await DBServices.instance.database;
    var res = await db.rawQuery(
      '''
      select username from ${DBServices.userTable} 
      where username = '${usernameController.text.toString()}'
      ''',
    );
    if (res.isEmpty) {
      add();
    } else {
      Get.snackbar(
        "Oops!",
        "Username sudah terpakai",
        backgroundColor: Colors.red,
        colorText: Colors.white,
        duration: Duration(seconds: 3),
      );
    }
  }

  changeVisibilityPassword() {
    isPasswordVisible = !isPasswordVisible;
    //update adalah method bawaan dari Framework GetX untuk mengupdate state
    update();
  }
}
