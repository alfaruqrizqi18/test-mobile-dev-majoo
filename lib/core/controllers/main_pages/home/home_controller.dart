import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/core/controllers/main_pages/favorites/favorites_controller.dart';
import 'package:test_majoo/core/controllers/main_pages/profile/profile_controller.dart';
import 'package:test_majoo/core/models/image_pexels_model.dart';
import 'package:test_majoo/services/api_services.dart';
import 'package:test_majoo/services/db_services.dart';
import 'package:test_majoo/static/api_url.dart';
import 'package:test_majoo/static/session_key.dart';

class HomeController extends GetxController {
  //variabel untuk data yang akan ditampilkan
  List<ImagePexelsModel> images = [];

  bool isListView = true;

  bool isDesc = true;

  @override
  void onInit() {
    super.onInit();
    checkIfLocalDatabaseNotEmpty();
  }

  checkIfLocalDatabaseNotEmpty() async {
    Database db = await DBServices.instance.database;
    var res = await db.rawQuery(
      '''
      select * from ${DBServices.imagesTable} 
      ''',
    );
    if (res.isEmpty) {
      getImagesFromPexels();
    } else {
      getImagesFromLocalDatabase();
    }
    Get.put(FavoritesController());
    Get.put(ProfileController());
  }

  getImagesFromPexels() async {
    ApiServices apiServices = ApiServices();
    apiServices.getDataV2(
      url: ApiUrl.curated,
      parameters: {},
    ).then((value) {
      var response = value['response'].data['photos'];
      for (int i = 0; i < response.length; i++) {
        ImagePexelsModel imagePexelsModel = ImagePexelsModel(
          id: response[i]['id'],
          width: response[i]['width'],
          height: response[i]['height'],
          url: response[i]['url'],
          photographer: response[i]['photographer'],
          photographerUrl: response[i]['photographer_url'],
          photographerId: response[i]['photographer_id'],
          avgColor: response[i]['avg_color'],
          original: response[i]['src']['original'],
          large2x: response[i]['src']['large2x'],
          large: response[i]['src']['large'],
          medium: response[i]['src']['medium'],
          small: response[i]['src']['small'],
          portrait: response[i]['src']['portrait'],
          landscape: response[i]['src']['landscape'],
          tiny: response[i]['src']['tiny'],
          liked: response[i]['liked'].toString(),
          alt: response[i]['alt'],
        );
        images.add(imagePexelsModel);
        saveImagesFromPexelsToLocalDatabase(imagePexelsModel);
      }
      update();
    });
  }

  getImagesFromLocalDatabase() async {
    images.clear();
    update();
    Database db = await DBServices.instance.database;
    var res = await db.rawQuery(
      '''
      select * from ${DBServices.imagesTable} 
      order by id ${isDesc ? "desc" : "asc"}
      ''',
    );
    if (res.isNotEmpty) {
      print(res.toList()[0]['']);
      for (int i = 0; i < res.length; i++) {
        ImagePexelsModel imagePexelsModel =
            ImagePexelsModel.fromJson(res.toList()[i]);
        images.add(imagePexelsModel);
      }
      update();
    }
    print("Images from local database : ${res.toList()}");
  }

  saveImagesFromPexelsToLocalDatabase(ImagePexelsModel imagePexelsModel) async {
    DBServices.instance.insert(
      tableName: DBServices.imagesTable,
      models: imagePexelsModel.toJson(),
    );
    Get.snackbar(
      "Yeay!",
      "Berhasil menyimpan foto dari Pexels.com",
      backgroundColor: Colors.green[300],
      colorText: Colors.white,
      duration: Duration(seconds: 3),
    );
  }

  removeImages() async {
    Database db = await DBServices.instance.database;
    await db.rawQuery("DELETE FROM ${DBServices.imagesTable}");
    images.clear();
    update();
    Get.snackbar(
      "Yeay!",
      "Berhasil menghapus foto dari database",
      backgroundColor: Colors.green[300],
      colorText: Colors.white,
      duration: Duration(seconds: 3),
    );
  }

  refreshData() {
    images.clear();
    update();
    checkIfLocalDatabaseNotEmpty();
  }

  changeView() {
    isListView = !isListView;
    update();
  }

  changeSort() {
    isDesc = !isDesc;
    update();
    images.clear();
    getImagesFromLocalDatabase();
  }

  addToFavorites({required int idImage}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = prefs.getInt(SessionKey.userId)!;
    Map<String, dynamic> param = {
      "id": Random().nextInt(999),
      "images_id": idImage,
      "user_id": userId,
    };
    print(param);
    DBServices.instance.insert(
      tableName: DBServices.favoriteImagesTable,
      models: param,
    );
    Get.find<FavoritesController>().getFavoriteImages();
    Get.snackbar(
      "Yeay!",
      "Berhasil menambahkan favorite baru",
      backgroundColor: Colors.green[300],
      colorText: Colors.white,
      duration: Duration(seconds: 3),
    );
  }

  checkIsFavorite({required int idImage}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = prefs.getInt(SessionKey.userId)!;
    Database db = await DBServices.instance.database;
    var res = await db.rawQuery(
      '''
      select * from ${DBServices.favoriteImagesTable}
      left join ${DBServices.imagesTable} on ${DBServices.imagesTable}.id = ${DBServices.favoriteImagesTable}.images_id 
      where images_id = $idImage 
      and user_id = $userId
      ''',
    );

    if (res.isEmpty) {
      addToFavorites(idImage: idImage);
    } else {
      Get.snackbar(
        "Oops!",
        "Kamu sudah memfavoritkan foto ini",
        backgroundColor: Colors.red,
        colorText: Colors.white,
        duration: Duration(seconds: 3),
      );
    }
    print(res.toList());
  }
}
