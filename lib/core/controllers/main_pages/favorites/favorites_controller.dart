import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/services/db_services.dart';
import 'package:test_majoo/static/session_key.dart';

class FavoritesController extends GetxController {
  //variabel untuk data yang akan ditampilkan
  List images = [];

  bool isListView = true;

  bool isDesc = true;

  @override
  void onInit() {
    super.onInit();
    getFavoriteImages();
  }

  getFavoriteImages() async {
    images.clear();
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = prefs.getInt(SessionKey.userId)!;
    Database db = await DBServices.instance.database;
    var res = await db.rawQuery(
      '''
      select * from ${DBServices.favoriteImagesTable}
      left join ${DBServices.imagesTable} on ${DBServices.imagesTable}.id = ${DBServices.favoriteImagesTable}.images_id 
      where user_id = $userId
      order by id ${isDesc ? "desc" : "asc"}
      ''',
    );
    if (res.isNotEmpty) {
      images.addAll(res.toList());
      update();
    }
  }

  changeView() {
    isListView = !isListView;
    update();
  }

  changeSort() {
    isDesc = !isDesc;
    update();
    images.clear();
    getFavoriteImages();
  }

  removeFromFavorite({required int imageId}) async {
    Database db = await DBServices.instance.database;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = prefs.getInt(SessionKey.userId)!;
    await db.rawQuery(
      '''
      delete from ${DBServices.favoriteImagesTable}
      where images_id = '$imageId'
      and user_id = $userId
      ''',
    );
    Get.snackbar(
      "Yeay!",
      "Berhasil menghapus foto dari favorite",
      backgroundColor: Colors.green[300],
      colorText: Colors.white,
      duration: Duration(seconds: 3),
    );
    getFavoriteImages();
  }
}
