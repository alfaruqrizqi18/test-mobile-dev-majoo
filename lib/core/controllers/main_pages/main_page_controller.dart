import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_majoo/core/views/main_pages/favorites/favorite_view.dart';
import 'package:test_majoo/core/views/main_pages/home/home_view.dart';
import 'package:test_majoo/core/views/main_pages/profile/profile_view.dart';

class MainPageController extends GetxController {
  //index halaman yang sedang tampil
  int selectedIndexPage = 0;

  List<Widget> pages = [
    HomeView(),
    FavoritesView(),
    ProfileView(),
  ];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  changePage(int index) {
    selectedIndexPage = index;
    update();
  }
}
