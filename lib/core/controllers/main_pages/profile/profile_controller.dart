import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/core/views/authentication/sign_in.dart';
import 'package:test_majoo/static/session_key.dart';

class ProfileController extends GetxController {
  Map<String, dynamic> userDetail = {};
  @override
  void onInit() {
    super.onInit();
    getUserDetail();
  }

  getUserDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userDetail.addAll({
      "username": prefs.getString(SessionKey.username),
      "name": prefs.getString(SessionKey.name),
    });
    update();
  }

  logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    Get.offAll(() => SignIn());
  }
}
