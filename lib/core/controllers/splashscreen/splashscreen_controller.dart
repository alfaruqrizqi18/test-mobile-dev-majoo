import 'dart:async';
import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/core/views/authentication/sign_in.dart';
import 'package:test_majoo/core/views/main_pages/main_page_view.dart';
import 'package:test_majoo/static/session_key.dart';

class SplashscreenController extends GetxController {
  bool isLoggedIn = false;

  @override
  void onInit() {
    getStatusLogin();
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  getStatusLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // check apakah shared preferences isLoggedIn tidak kosong?
    if (prefs.getBool(SessionKey.isLoggedIn) != null) {
      // jika tidak kosong maka, maka shared preferences isLoggedIn akan di cek
      // apakah valuenya true atau false
      if (prefs.getBool(SessionKey.isLoggedIn)!) {
        isLoggedIn = true;
      } else {
        isLoggedIn = false;
      }
    }
    update();
    onDoneLoading();

    print("Logged in ${prefs.getBool(SessionKey.isLoggedIn)}");
    print("username in ${prefs.getString(SessionKey.username)}");
    print("id user in ${prefs.getInt(SessionKey.userId)}");
  }

  onDoneLoading() {
    // jika isLoggedIn sudah bernilai true
    // maka akan redirect ke main pages
    // jika false akan redirect ke signIn
    Timer(Duration(milliseconds: 2000), () async {
      if (isLoggedIn) {
        Get.offAll(() => MainPageView());
      } else {
        Get.offAll(() => SignIn());
      }
    });
  }
}
