import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBServices {
  static final _databaseName = "majoo.db";
  static final _databaseVersion = 1;

  static final userTable = "users";
  static final imagesTable = "images";
  static final favoriteImagesTable = "favorite_images";

  DBServices._privateConstructor();
  static final DBServices instance = DBServices._privateConstructor();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $userTable (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name STRING NULL,
            username STRING NULL,
            password STRING NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE $imagesTable (
            id INTEGER PRIMARY KEY,
            width INTEGER NULL,
            height INTEGER NULL,
            url STRING NULL,
            photographer STRING NULL,
            photographer_url STRING NULL,
            photographer_id INTEGER NULL,
            avg_color STRING NULL,
            original STRING NULL,
            large2x STRING NULL,
            large STRING NULL,
            medium STRING NULL,
            small STRING NULL,
            portrait STRING NULL,
            landscape STRING NULL,
            tiny STRING NULL,
            liked STRING NULL,
            alt STRING NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE $favoriteImagesTable (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            images_id INTEGER NULL,
            user_id INTEGER NULL
          )
          ''');
  }

  Future<int> insert(
      {required String tableName, required Map<String, dynamic> models}) async {
    Database db = await instance.database;
    var res = db.insert(tableName, models);
    return res;
  }

  Future<List<Map<String, dynamic>>> queryAllRows(
      {required String tableName, required String orderBy}) async {
    Database db = await instance.database;
    var res = await db.query(tableName, orderBy: orderBy);
    return res;
  }

  // Future<int> updateData(
  //   int id, {
  //   required String bookTitle,
  //   required String authorName,
  //   required String pubDate,
  // }) async {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['book_title'] = bookTitle;
  //   data['author_name'] = authorName;
  //   data['publication_date'] = pubDate;
  //   Database db = await instance.database;
  //   var res = await db.update(
  //     table,
  //     data,
  //     where: '$columnId = ?',
  //     whereArgs: [id],
  //   );
  //   return res;
  // }

  // Future<List<Map<String, dynamic>>> getById(int id) async {
  //   Database db = await instance.database;
  //   return await db.query(
  //     table,
  //     where: '$columnId = ?',
  //     whereArgs: [id],
  //   );
  // }

  Future<int> delete({required int id, required String tableName}) async {
    Database db = await instance.database;
    return await db.delete(tableName, where: 'id = ?', whereArgs: [id]);
  }

  Future clearTable() async {
    Database db = await instance.database;
    await db.rawQuery("DELETE FROM $userTable");
    await db.rawQuery("DELETE FROM $imagesTable");
    await db.rawQuery("DELETE FROM $favoriteImagesTable");
  }
}
